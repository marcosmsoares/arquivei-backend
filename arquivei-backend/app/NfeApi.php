<?php

namespace App;

class NfeApi {

    // STATUS CODE
    const STATUS_SUCCESS = 200;
    
    //CREDENTIALS
    const API_ID = "f96ae22f7c5d74fa4d78e764563d52811570588e";
    const API_KEY = "cc79ee9464257c9e1901703e04ac9f86b0f387c2";

    //ROUTES
    const BASE_URL = "https://sandbox-api.arquivei.com.br";
    const NFE_RECEIVED = "/v1/nfe/received";

    private function request($route){

        $endpoint = self::BASE_URL . $route;
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint, ['headers' => [
            'Content-Type'    => 'application/json',
            'x-api-id'  => self::API_ID,
            'x-api-key' => self::API_KEY
        ]]);
        
        $statusCode = $response->getStatusCode();

        if($statusCode !== self::STATUS_SUCCESS){
            throw new \Exception("Não foi possível recuperar os documentos NFE. Por favor, novamente mais tarde.", $statusCode);
        }

        $content = json_decode($response->getBody());

        return $content;
    }

    /**
     * Get the received nfe documents
     */
    public function getReceived(){
        return $this->request(self::NFE_RECEIVED);
    }


}