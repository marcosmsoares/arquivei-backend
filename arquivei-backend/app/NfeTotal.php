<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $access_key
 * @property float $vBC
 * @property float $vICMS
 * @property float $vICMSDeson
 * @property float $vBCST
 * @property float $vST
 * @property float $vProd
 * @property float $vFrete
 * @property float $vSeg
 * @property float $vDesc
 * @property float $vII
 * @property float $vIPI
 * @property float $vPIS
 * @property float $vCOFINS
 * @property float $vOutro
 * @property float $vNF
 */
class NfeTotal extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'nfe_total';

    /**
     * @var array
     */
    protected $fillable = ['access_key', 'vBC', 'vICMS', 'vICMSDeson', 'vBCST', 'vST', 'vProd', 'vFrete', 'vSeg', 'vDesc', 'vII', 'vIPI', 'vPIS', 'vCOFINS', 'vOutro', 'vNF'];

    public static function getValueByAccessKey($access_key, $attr){

        if(!isset($access_key) || $access_key == ""){
            throw new \Exception("Invalid Access Key", 400);
        }

        $self = self::where('access_key', '=', $access_key)->first();

        if(!isset($self)){
            throw new \Exception("Access Key not found", 404);
        }
        
        return isset($self->{$attr}) ? $self->{$attr} : 'Não informado';
    }
}
