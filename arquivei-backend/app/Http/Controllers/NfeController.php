<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\NfeApi;
use App\NfeXmlFormatter;
use App\NfeTotal;
use Illuminate\Support\Facades\Auth;

class NfeController extends Controller
{

    /**
     * Funcionalidade responsável retornar o valor total da nota (vNF) através da chave de acesso
     */
    public function getNfeTotalVnf(Request $request){

        try {

            $access_key = $request->input('access_key');
            $vnf = NfeTotal::getValueByAccessKey($access_key, 'vNF');
            $data = ['access_key' => $access_key, 'vNF' => $vnf];

            $resp = new \stdClass();
            $resp->title = 'vNF value';
            $resp->data = $data;

            return response()->json($resp, 200);

        } catch (\Exception $ex){
            return response()->json($ex->getMessage(), $ex->getCode());
        }
    }

    /**
     * Funcionalidade responsável por recuperar as notas recebidas da API
     * Carrega as notas no modelo NfeXmlFormatter
     * Salva no banco os valores referente ao total da nota
     */
    public function saveNfeTotal(){
        
        try {

            $nfeApi = new NfeApi();
            $received = $nfeApi->getReceived();

            foreach ($received->data as $key => $value) {
                
                $nfeXmlFormatter = new NfeXmlFormatter($value->xml);
                $accessKey = $value->access_key;
                $icmsTot = $nfeXmlFormatter->getICMSTot();
                
                $nfe_total = new NfeTotal();
                $nfe_total->access_key = $accessKey;
                $nfe_total->vBC = $nfeXmlFormatter->getVBC();
                $nfe_total->vICMS = $nfeXmlFormatter->getVICMS();
                $nfe_total->vICMSDeson = $nfeXmlFormatter->getVICMSDeson();
                $nfe_total->vBCST = $nfeXmlFormatter->getVBCST();
                $nfe_total->vST = $nfeXmlFormatter->getVST();
                $nfe_total->vProd = $nfeXmlFormatter->getVProd();
                $nfe_total->vFrete = $nfeXmlFormatter->getVFrete();
                $nfe_total->vSeg = $nfeXmlFormatter->getVSeg();
                $nfe_total->vDesc = $nfeXmlFormatter->getVDesc();
                $nfe_total->vII = $nfeXmlFormatter->getVII();
                $nfe_total->vIPI = $nfeXmlFormatter->getVIPI();
                $nfe_total->vPIS = $nfeXmlFormatter->getVPIS();
                $nfe_total->vCOFINS = $nfeXmlFormatter->getVCOFINS();
                $nfe_total->vOutro = $nfeXmlFormatter->getVOutro();
                $nfe_total->vNF = $nfeXmlFormatter->getVNF();

                $nfe_total->save();
            }

            return response()->json('Operação concluída', 200);

        } catch(\Exception $ex){
            return response()->json($ex->getMessage(), 400);
        }

    }
}
