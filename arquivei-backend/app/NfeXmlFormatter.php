<?php
namespace App;

class NfeXmlFormatter {

    private $NFe;
    private $infNFe;
    private $ide;
    private $emit;
    private $dest;
    private $det;
    private $total;
    private $ICMSTot;
    private $vBC;
    private $vICMS;
    private $vICMSDeson;
    private $vBCST;
    private $vST;
    private $vProd;
    private $vFrete;
    private $vSeg;
    private $vDesc;
    private $vII;
    private $vIPI;
    private $vPIS;
    private $vCOFINS;
    private $vOutro;
    private $vNF;
    private $transp;
    private $signature;
    private $protNFe;
    private $infProt;

    public function __construct($xml){
        $this->load($xml);
    }
    
    /**
     * Funcionalidade responsável por decodificar o xml recebido
     */
    private function decode($encodedXml){
        return simplexml_load_string(base64_decode($encodedXml));
    }

    /**
     * Funcionalidade responsável por carregar as variáveis da nota recebida nas variáveis da classe
     */
    private function load($xml){
        
        $xml = $this->decode($xml);

        $this->NFe         = isset($xml->NFe) ? $xml->NFe : null;
        $this->protNFe     = isset($xml->protNFe) ? $xml->protNFe : null;
        $this->infProt     = isset($xml->protNFe->infProt) ? $xml->protNFe->infProt : null;
        $this->infNFe      = isset($this->NFe->infNFe) ? $this->NFe->infNFe : null;
        $this->ide         = isset($this->infNFe->ide) ? $this->infNFe->ide : null;
        $this->emit        = isset($this->infNFe->emit) ? $this->infNFe->emit : null;
        $this->dest        = isset($this->infNFe->dest) ? $this->infNFe->dest : null;
        $this->det         = isset($this->infNFe->det) ? $this->infNFe->det : null;
        $this->transp      = isset($this->infNFe->transp) ? $this->infNFe->transp : null;
        $this->total       = isset($this->infNFe->total) ? $this->infNFe->total : null;
        $this->ICMSTot     = isset($this->total->ICMSTot) ? $this->total->ICMSTot : null;
        $this->vBC         = isset($this->ICMSTot->vBC) ? $this->ICMSTot->vBC : null;
        $this->vICMS       = isset($this->ICMSTot->vICMS) ? $this->ICMSTot->vICMS : null;
        $this->vICMSDeson  = isset($this->ICMSTot->vICMSDeson) ? $this->ICMSTot->vICMSDeson : null;
        $this->vBCST       = isset($this->ICMSTot->vBCST) ? $this->ICMSTot->vBCST : null;
        $this->vST         = isset($this->ICMSTot->vST) ? $this->ICMSTot->vST : null;
        $this->vProd       = isset($this->ICMSTot->vProd) ? $this->ICMSTot->vProd : null;
        $this->vFrete      = isset($this->ICMSTot->vFrete) ? $this->ICMSTot->vFrete: null;
        $this->vSeg        = isset($this->ICMSTot->vSeg) ? $this->ICMSTot->vSeg : null;
        $this->vDesc       = isset($this->ICMSTot->vDesc) ? $this->ICMSTot->vDesc : null;
        $this->vII         = isset($this->ICMSTot->vII) ? $this->ICMSTot->vII : null;
        $this->vIPI        = isset($this->ICMSTot->vIPI) ? $this->ICMSTot->vIPI : null;
        $this->vPIS        = isset($this->ICMSTot->vPIS) ? $this->ICMSTot->vPIS : null;
        $this->vCOFINS     = isset($this->ICMSTot->vCOFINS) ? $this->ICMSTot->vCOFINS : null;
        $this->vOutro      = isset($this->ICMSTot->vOutro) ? $this->ICMSTot->vOutro : null;
        $this->vNF         = isset($this->ICMSTot->vNF) ? $this->ICMSTot->vNF : null;
    }
    
    /**
     * Get the value of ICMSTot
     */ 
    public function getICMSTot()
    {
        return $this->ICMSTot;
    }

    /**
     * Get the value of vBC
     */ 
    public function getVBC()
    {
        return $this->vBC;
    }

    /**
     * Get the value of vICMS
     */ 
    public function getVICMS()
    {
        return $this->vICMS;
    }

    /**
     * Get the value of vICMSDeson
     */ 
    public function getVICMSDeson()
    {
        return $this->vICMSDeson;
    }

    /**
     * Get the value of vBCST
     */ 
    public function getVBCST()
    {
        return $this->vBCST;
    }

    /**
     * Get the value of vST
     */ 
    public function getVST()
    {
        return $this->vST;
    }

    /**
     * Get the value of vProd
     */ 
    public function getVProd()
    {
        return $this->vProd;
    }

    /**
     * Get the value of vFrete
     */ 
    public function getVFrete()
    {
        return $this->vFrete;
    }

    /**
     * Get the value of vSeg
     */ 
    public function getVSeg()
    {
        return $this->vSeg;
    }

    /**
     * Get the value of vDesc
     */ 
    public function getVDesc()
    {
        return $this->vDesc;
    }

    /**
     * Get the value of vII
     */ 
    public function getVII()
    {
        return $this->vII;
    }

    /**
     * Get the value of vIPI
     */ 
    public function getVIPI()
    {
        return $this->vIPI;
    }

    /**
     * Get the value of vPIS
     */ 
    public function getVPIS()
    {
        return $this->vPIS;
    }

    /**
     * Get the value of vCOFINS
     */ 
    public function getVCOFINS()
    {
        return $this->vCOFINS;
    }

    /**
     * Get the value of vOutro
     */ 
    public function getVOutro()
    {
        return $this->vOutro;
    }

    /**
     * Get the value of vNF
     */ 
    public function getVNF()
    {
        return $this->vNF;
    }
}