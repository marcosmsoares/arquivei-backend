<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\NfeApi;
use App\NfeXmlFormatter;
use App\NfeTotal;

class NfeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nfe:saveReceived';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save all nfes received';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $nfeApi = new NfeApi();
            $received = $nfeApi->getReceived();

            foreach ($received->data as $key => $value) {
                
                $nfeXmlFormatter = new NfeXmlFormatter($value->xml);
                $accessKey = $value->access_key;
                $icmsTot = $nfeXmlFormatter->getICMSTot();
                
                $nfe_total = new NfeTotal();
                $nfe_total->access_key = $accessKey;
                $nfe_total->vBC = $nfeXmlFormatter->getVBC();
                $nfe_total->vICMS = $nfeXmlFormatter->getVICMS();
                $nfe_total->vICMSDeson = $nfeXmlFormatter->getVICMSDeson();
                $nfe_total->vBCST = $nfeXmlFormatter->getVBCST();
                $nfe_total->vST = $nfeXmlFormatter->getVST();
                $nfe_total->vProd = $nfeXmlFormatter->getVProd();
                $nfe_total->vFrete = $nfeXmlFormatter->getVFrete();
                $nfe_total->vSeg = $nfeXmlFormatter->getVSeg();
                $nfe_total->vDesc = $nfeXmlFormatter->getVDesc();
                $nfe_total->vII = $nfeXmlFormatter->getVII();
                $nfe_total->vIPI = $nfeXmlFormatter->getVIPI();
                $nfe_total->vPIS = $nfeXmlFormatter->getVPIS();
                $nfe_total->vCOFINS = $nfeXmlFormatter->getVCOFINS();
                $nfe_total->vOutro = $nfeXmlFormatter->getVOutro();
                $nfe_total->vNF = $nfeXmlFormatter->getVNF();

                $nfe_total->save();
            }

            return 'ok';

        } catch(\Exception $ex){
            return 'não foi possível concluir a operação';
        }
    }
}
