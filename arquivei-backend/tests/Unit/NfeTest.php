<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

// STATUS CODE
const STATUS_SUCCESS = 200;

//CREDENTIALS
const API_ID = "f96ae22f7c5d74fa4d78e764563d52811570588e";
const API_KEY = "cc79ee9464257c9e1901703e04ac9f86b0f387c2";

//ROUTES
const BASE_URL = "https://sandbox-api.arquivei.com.br";
const NFE_RECEIVED = "/v1/nfe/received";

class NfeTest extends TestCase
{
    /**
     * Unit test connection.
     *
     * @return void
     */
    public function testConnect()
    {
        $endpoint = 'https://sandbox-api.arquivei.com.br/v1/nfe/received';
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint, ['headers' => [
            'Content-Type'    => 'application/json',
            'x-api-id'  => 'f96ae22f7c5d74fa4d78e764563d52811570588e',
            'x-api-key' => 'cc79ee9464257c9e1901703e04ac9f86b0f387c2'
        ]]);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
