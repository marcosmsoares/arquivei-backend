<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NfeTotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfe_total', function (Blueprint $table) {
            $table->bigIncrements('id')->unique()->autoIncrement();
            $table->string('access_key')->unique();
            $table->float('vBC')->nullable();
            $table->float('vICMS')->nullable();
            $table->float('vICMSDeson')->nullable();
            $table->float('vBCST')->nullable();
            $table->float('vST')->nullable();
            $table->float('vProd')->nullable();
            $table->float('vFrete')->nullable();
            $table->float('vSeg')->nullable();
            $table->float('vDesc')->nullable();
            $table->float('vII')->nullable();
            $table->float('vIPI')->nullable();
            $table->float('vPIS')->nullable();
            $table->float('vCOFINS')->nullable();
            $table->float('vOutro')->nullable();
            $table->float('vNF')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nfe_total');
    }
}
