## Objetivo & Estrutura

Uma aplicação Laravel que tem como objetivo realizar uma integração com a api de notas recebidas da empresa Arquivei, decodificar essas notas e salvar no banco de dados o valor total da nota. A aplicação também disponibiliza uma rota para recuperar o valor total através da chave de acesso.

Classes importantes da aplicação:

1. Class NfeApi -> Classe responsável por realizar a integração com a api da Arquivei.
2. Class NfeXmlFormatter -> Classe responsável por decodificar as notas recebidas e servir de modelo para as mesmas, facilitando o acesso a qualquer atributo da nota.
3. NfeTotal -> Model Class responsável por espelhar a tabela que guarda os valores de uma nota recebida.

*Essa estrutura foi montada visando a escalabilidade, para que se possa criar novas funcionalidades que envolvem uma nota recebida.*

## Dependências 

**Utilizando Docker (recomendado):**

1. Docker
2. Docker Compose

**Dependências para que a aplicação possa ser executada sem o Docker.**

1. PHP >= 7.0.0
2. OpenSSL PHP Extension
3. PDO PHP Extension
4. Mbstring PHP Extension
5. Tokenizer PHP Extension
6. XML PHP Extension
7. Composer
8. MySQL Server

---

## Instalação & configuração

**Utilizando o Docker (recomendado)**

A aplicação utiliza o Docker Compose, o que facilita a criação de toda a infraestrutura necessária para executar a aplicação em um único arquivo.

1. Clone do projeto e entrar na pasta do laravel *cd arquivei-backend*
2. Utilize a imagem do composer para instalar as dependências sem precisar instalar o composer globalmente utilizando o comando *docker run --rm -v $(pwd):/app composer install*
3. Crie o arquivo .env a partir do .env.example com o comando *cp .env.example .env*
4. No arquivo docker-compose.yml atualize a senha para acesso root ao banco de dados no parâmetro **MYSQL_ROOT_PASSWORD**.
5. Execute o comando *docker-compose up -d* que inciará todos os contêiners, volumes e configurar a conexão entre os contêiners.
6. Após a conclusão do processo utilize o comando *docker ps* para analisar os contêiners criados.
7. Agora, iremos atualizar as configurações necessárias para a conexão com o banco de dados. Execute o comando *docker-compose exec app nano .env* e atualize as configurações de acesso ao banco.
    * DB_CONNECTION=mysql
    * DB_HOST=db (contêiner de banco de dados)
    * DB_PORT=3306
    * DB_DATABASE=arquivei (previamente definida no arquivo docker-compose.yml)
    * DB_USERNAME=arquiveiuser (vamos criar o usuário para o mysql no passo 8)
    * DB_PASSWORD=arquivei2020 (vamos criar uma senha para o mysql no passo 8)
8. Utilize o comando *docker-compose exec db bash* para acessar o bash do contêiner do banco de dados. Execute o comando *mysql -u root -p* para acessar na conta administrativa **root**, insira a senha que você definiu no passo 3. Agora iremos criar o usuário executando os seguintes comandos:
    * GRANT ALL ON arquivei.* TO 'arquiveiuser'@'%' IDENTIFIED BY 'arquivei2020';
    * FLUSH PRIVILEGES;
9. Utilize o comando *docker-compose exec app php artisan migrate* para migrar as tabelas que iremos usar na aplicação para o banco de dados.
10. Pronto para testar as rotas! 


**Sem o a utilização do Docker:**

Passo a passo para instalação das dependências, executar o migration para gerar as tabelas no banco de dados e executar o projeto no servidor local

1. Clone do projeto e entrar na pasta do laravel *cd arquivei-backend*
2. Utilize a imagem do composer para instalar as dependências sem precisar instalar o composer 
3. Utilize o comando *composer update* para instalar as dependências requeridas
4. Crie um arquivo .env de configuração do projeto. Sugestão: crie o arquivo .env a partir do .env.example com o comando *cp .env.example .env* e depois gere a chave da sua aplicação com o comando *php artisan key:generate*
5. Configure suas credenciais do banco de dados no arquivo .env
6. Agora iremos gerar as tabelas necessárias para o funcionamento da aplicação executando o comando *php artisan migrate*
7. Por fim, para executar a aplicação no servidor local execute o comando *php artisan serve*.
8. Pronto para testar as rotas!

---

## Rotas

Rotas da aplicação

1. Recuperar notas da api da Arquivei e salvar dados referente ao valor total da nota no banco de dados.
    * method: POST
    * route: /api/nfe/saveTotal


2. Recuperar valor total da nota através da chave de acesso.
    * method: GET
    * route: /api/nfe/getVnf
    * params:
        * access_key (string)

---

## Outra opção para conexão com API e salvar as notas recebidas

Foi criado um Command no laravel para executar esta ação, podemos utilizar esse comando num shell script para ser executando numa CRON, por exemplo

Comando: **php artisan nfe:saveReceived**

---

## Testes Unitários

Foi criado um simples teste unitário para verificar a conexão com a API da Arquivei.

Comando: **./vendor/bin/phpunit**

---

Obrigado :)